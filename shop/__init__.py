from flask import Flask
from logging.config import dictConfig
import psycopg2

DB = "postgres"
HOST = "127.0.0.1"
USER = "postgres"
PWD = "root"

dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s][%(levelname)s] in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})

app = Flask(__name__)
conn = psycopg2.connect(dbname=DB, user=USER, password=PWD, host=HOST)
app.logger.info("Connect to db %s successfully", DB)